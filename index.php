<?php
	/*
 	* Tesltra 2019 Graduate Program
	*	Case Study - Coding
 	* @author Flavio Klages
 	*/
	//define cache
	$cache = 'messages/file.cache.php';//define where to stored the cache file directory
	$time = '30 seconds';//set time of file live (30 seconds)
	if(file_exists($cache))//checking if file exists 
	{
		//echo 'Caching... 123  ';//message to display in case reloading page 
		include $cache;//cache included to cache file
	}
	else
	{
		echo 'Cache file not found = ';//message fail to file cache		
		$fh = fopen($cache, 'w') or die ('error writing file');//open cache and write on it / or if not able to write cache will die
		fwrite($fh,$json_response);//writing on file 
		fclose($fh);//cache close
	}
		//process client request (via url)
		header("Content-Type:application/json");//response in Json format
		include("functions.php");//function to display data (message)
		if(!empty($_GET['id']))//checking if id exist API
		{
			//return request
			$id=$_GET['id'];
			$mess=get_id($id);
				
			if(empty($mess))
				//False : id not found 
				deliver_response(404, "ERROR: Id not found", NULL);
			else
				//True : return output message
				deliver_response($id, "Id found",$mess);
				
		}
		else
		{
			//throw invalid request
			deliver_response(404, "ERROR: Invalid Request", NULL);
		}	
?>